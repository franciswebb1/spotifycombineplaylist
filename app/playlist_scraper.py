#!/usr/bin/env python3
import requests
import pandas as pd

from resources.secrets import spotify_token, spotify_user_id


class ScrapePlaylist:
    def __init__(self):
        self.all_song_info = {}

    def get_playlist_uris(self, name, offset):
        query = "https://api.spotify.com/v1/search?q=%22{}%22&type=playlist&limit=50&offset={}".format(name, offset)
        response = requests.get(
            query,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )
        response_json = response.json()
        if response.status_code != 200:
            return 0
        uris = []
        for i in range(len(response_json['playlists']['items'])):
            uris += [response_json['playlists']['items'][i]['id']]
        return uris

    def get_spotify_playlist_track_uris(self, playlist_id):
        query = "https://api.spotify.com/v1/users/{}/playlists/{}/tracks".format(spotify_user_id, playlist_id)
        response = requests.get(
            query,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )
        response_json = response.json()
        uris = []
        for i in range(len(response_json['items'])):
            id = response_json['items'][i]['track']['id']
            artist = response_json['items'][i]['track']['artists'][0]['name']
            name = response_json['items'][i]['track']['name']
            uris += [[id, artist, name]]
        return uris

    def get_track_name_and_artist(self, id):
        query = "https://api.spotify.com/v1/tracks/{}".format(id)

        response = requests.get(
            query,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )
        response_json = response.json()
        return (response_json['artists'][0]['name'], response_json['name'])

    def track_artist_count_dict(self, name, index): # name = name of playlist, index = number of playlists
        playlistUris = []
        i = 0
        while i < index:
            try:
                playlistUris += self.get_playlist_uris(name=name, offset=i)
            except:
                a = 2
            i += 50

        columns = ['track_id', 'artist', 'track', 'count']
        trackDf = pd.DataFrame(columns=columns)
        trackDf.set_index('track_id', inplace=True)
        trackDf.index.name = 'track_id'
        for playlistUri in playlistUris:
            try:
                trackUris = self.get_spotify_playlist_track_uris(playlistUri)
                for trackUri in trackUris:
                    if trackUri[0] in trackDf.index:
                        trackDf.loc[trackUri[0]]['count'] += 1
                    else:
                        id = trackUri[0]
                        artist = trackUri[1]
                        track = trackUri[2]
                        trackDf.loc[id] = [artist, track, 1]
            except:
                pass
        return trackDf


class TrackAnalysis:
    def __init__(self):
        self.tracks = {}

    def sort_df(self, df):
        filtered_df = df[df['count'] > 0]
        filtered_df.sort_values(by=['count'], ascending=False, inplace=True)
        return filtered_df

