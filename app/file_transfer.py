import boto3
import resources.secrets as secrets


class FileTransfer:
    def __init__(self, tablename, object={}, localhost=False):
        self.tablename = tablename
        self.localhost = localhost
        self.object = object

    def get_table_metadata(self, db_resource):
        table = db_resource.Table(self.tablename)

        return {
            'num_items': table.item_count,
            'primary_key_name': table.key_schema[0],
            'status': table.table_status,
            'bytes_size': table.table_size_bytes,
            'global_secondary_indices': table.global_secondary_indexes
        }

    def delete_table(self, db_resource):
        table = db_resource.Table(self.tablename)
        table.delete()

    def create_track_count_table(self, db_resource):
        db_resource.create_table(
            TableName=self.tablename,
            KeySchema=[
                {
                    'AttributeName': 'track_id',
                    'KeyType': 'HASH'  # Partition key
                },
                {
                    'AttributeName': 'track',
                    'KeyType': 'RANGE'  # Partition key
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'track_id',
                    'AttributeType': 'S'  # Partition key
                },
                {
                    'AttributeName': 'track',
                    'AttributeType': 'S'
                },

            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 10,
                'WriteCapacityUnits': 10
            }
        )

    def write_object(self, db_resource):
        dynamoTable = db_resource.Table(self.tablename)
        for item in self.object:
            dynamoTable.put_item(
                Item=item
            )

    def file_transfer(self):
        if self.localhost:
            dynamodb = boto3.resource('dynamodb',
                                      endpoint_url='http://localhost:8000',
                                      region_name=secrets.AWS_DEFAULT_REGION,
                                      aws_access_key_id=secrets.AWS_ACCESS_KEY_ID,
                                      aws_secret_access_key=secrets.AWS_SECRET_ACCESS_KEY,
                                      )
        else:
            dynamodb = boto3.resource('dynamodb',
                                      region_name=secrets.AWS_DEFAULT_REGION,
                                      aws_access_key_id=secrets.AWS_ACCESS_KEY_ID,
                                      aws_secret_access_key=secrets.AWS_SECRET_ACCESS_KEY,
                                      )
        self.create_track_count_table(db_resource=dynamodb)
        print(self.get_table_metadata(db_resource=dynamodb))
        self.write_object(db_resource=dynamodb)
        print(self.get_table_metadata(db_resource=dynamodb))
