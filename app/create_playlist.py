#!/usr/bin/env python3
import json
import requests
import numpy as np
import pandas as pd

from resources.secrets import spotify_token, spotify_user_id


class CreatePlaylist:

    def __init__(self):
        self.all_song_info = {}

    def run(self):
        month_uris = self.get_playlist_uris()
        for month in month_uris:
            track_uris = self.get_spotify_playlist_track_uris(month)
            self.add_tracks_to_playlist(track_uris, '0VgXS1aZ49nnT7UI9oqTPd')
        return 0

    def get_playlist_uris(self):
        query = "https://api.spotify.com/v1/users/{}/playlists".format(spotify_user_id)
        response = requests.get(
            query,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )
        response_json = response.json()
        if response.status_code != 200:
            print('Error fetching playlist uris')
            return 0
        uris = []
        for i in range(len(response_json['items'])):
            if response_json['items'][i]['name'][-4:] == '2019':
                uris += [response_json['items'][i]['id']]
        return uris

    def get_spotify_playlist_track_uris(self, playlist_id):
        query = "https://api.spotify.com/v1/users/{}/playlists/{}/tracks".format(spotify_user_id, playlist_id)
        response = requests.get(
            query,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )
        response_json = response.json()
        uris = []
        for i in range(len(response_json['items'])):
            uris += ['spotify:track:' + response_json['items'][i]['track']['id']]
        return uris

    def get_track_uri(self, song_name, artist):
        query = f"https://api.spotify.com/v1/search?query=track%3A{song_name}+artist%3A{artist}&type=track&offset=0&limit=20"
        response = requests.get(
            query,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )
        response_json = response.json()
        if "tracks" in response_json:
            songs = response_json["tracks"]["items"]

            if len(songs) > 0:
                uri = songs[0]["uri"]
                return uri
            else:
                return ''
        else:
            return ''

    def add_tracks_to_playlist(self, uris, playlist_id):
        request_data = json.dumps(uris)

        query = "https://api.spotify.com/v1/users/{}/playlists/{}/tracks".format(spotify_user_id, playlist_id)

        response = requests.post(
            query,
            data=request_data,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )

        if response.status_code != 201:
            print('Error retrieving song features..')
            return 0

        response_json = response.json()
        return response_json

    def create_new_playlst(self, playlist_name):
        request_data = json.dumps({"name": f"{playlist_name}", "public": True})
        query = f"https://api.spotify.com/v1/users/{spotify_user_id}/playlists"

        response = requests.post(
            query,
            data=request_data,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )

        if response.status_code != 201:
            print('Error creating new playlist..')
            return 0

        response_json = response.json()
        return response_json['id']

    def get_show_average_data(self, uris):
        ids_array = uris.str.replace('spotify:track:', '').tolist()
        ids = ','.join(ids_array)
        query = f"https://api.spotify.com/v1/audio-features?ids={ids}"

        response = requests.get(
            query,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )

        if response.status_code != 200:
            print('Error creating new playlist..')
            return 0

        response_json = response.json()
        return pd.DataFrame(response_json['audio_features'])

    def add_uris_to_lib(self, library):
        track_uris = []
        for index, row in library.iterrows():
            track_uri = self.get_track_uri(song_name=row['Song'], artist=row['Artist'])
            track_uris.append(track_uri)
        library['track_uri'] = np.array(track_uris)
        return library

    def find_track_uris_for_nts_host(self, library, show_name):
        track_uris = []
        with open(f'{show_name}_track_uris.csv', 'a') as fd:
            fd.write('index,track_uri\n')
            for index, row in library.iterrows():
                track_uri = self.get_track_uri(song_name=row['Song'], artist=row['Artist'])
                if track_uri != '':
                    track_uris.append(track_uri)
                    fd.write(str(index) + ',' + track_uri + '\n')
        return pd.DataFrame(data=track_uris, columns=['track_uri'])

    def filter_unknowns(self, library):
        return library[(library.Song != 'Unknown') & (library.Artist != 'Unknown')]

    def fetch_track_uris(self, show_name, track_uris_csv_exists=False):
        if not track_uris_csv_exists:
            # this is the song, artist, date library NOT URIs
            library = pd.read_csv(f'{show_name}.csv', skiprows=None)
            library.columns = ['Date', 'Artist', 'Song']
            filtered_library = self.filter_unknowns(library=library)
            track_uris = self.find_track_uris_for_nts_host(library=filtered_library, show_name=show_name)
        else:
            track_uris = pd.read_csv(f'{show_name}_track_uris.csv')
        track_uris.drop_duplicates(subset=['track_uri'], inplace=True)
        track_uris = track_uris['track_uri'].tolist()
        return track_uris

    def gen_playlist_for_nts_host(self, show_name, track_uris_csv_exists=False):
        track_uris = self.fetch_track_uris(show_name=show_name, track_uris_csv_exists=track_uris_csv_exists)
        length = int(len(track_uris))
        playlist_id = cp.create_new_playlst(playlist_name=show_name)
        i = 0
        while i < length:
            cp.add_tracks_to_playlist(uris=track_uris[i:i + 99], playlist_id=playlist_id)
            i += 100


if __name__ == '__main__':
    cp = CreatePlaylist()
    show_name = "ruf-dug"
    #library = pd.read_csv(f'{show_name}.csv')
    #tracks_with_uris = cp.add_uris_to_lib(library=library)
    #tracks_with_uris.to_csv(f'{show_name}_tracks_with_uris.csv')
    #cp.gen_playlist_for_nts_host(show_name=show_name, track_uris_csv_exists=True)
    tracks_with_uris = pd.read_csv(f'{show_name}_tracks_with_uris.csv')
    dates = tracks_with_uris['Date'].unique()
    columns = ["danceability", "energy", "key", "loudness", "mode", "speechiness", "acousticness", "instrumentalness",
               "liveness", "valence", "tempo"]
    show_averages = pd.DataFrame(columns=columns, index=dates)
    for date in dates:
        tracks = tracks_with_uris[tracks_with_uris['Date'] == date]
        features = cp.get_show_average_data(uris=tracks['track_uri'].dropna())
        for col in columns:
            show_averages.loc[date][col] = features[col].mean()
    print(show_averages)

