FROM python:3.8-slim-buster

RUN mkdir /app
WORKDIR /app

RUN pip install --upgrade pip
COPY ./requirements.txt ./
RUN pip install -r ./requirements.txt

COPY / /
COPY /resources/secrets.py /resources/secrets.py

CMD [ "python", "/main.py" ]