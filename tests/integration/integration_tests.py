import app.create_playlist as cp
import unittest
import os
import pandas as pd


class TestURIs(unittest.TestCase):
    def test_find_track_uris(self):
        tracks = {'Song': ['Pigs... (In There)', 'Chika (Scared)', 'Stop the Dams'],
                  'Artist': ['Robert Wyatt', 'Fred again..', 'Gorillaz']}
        tracks_df = pd.DataFrame(tracks)
        URIs = ['spotify:track:33HqYo8b3I9xPdEALEdFFi', 'spotify:track:4pDE8vhtcEb90Eq2SvdWyB',
                'spotify:track:72nAHrq1bR6iDvng3qMpVf']
        test_track_uris = []
        CP = cp.CreatePlaylist()
        for index, row in tracks_df.iterrows():
            test_track_uris.append(CP.get_track_uri(song_name=row['Song'], artist=row['Artist']))
        self.assertEqual(URIs, test_track_uris)

    def test_mispelled_track_returns_nothing(self):
        tracks = {'Song': ['Pigs... (In There)?'],
                  'Artist': ['Rosdfsdfsdbert Wydsfsdfatt']}
        tracks_df = pd.DataFrame(tracks)
        test_track_uris = []
        CP = cp.CreatePlaylist()
        for index, row in tracks_df.iterrows():
            test_track_uris.append(CP.get_track_uri(song_name=row['Song'], artist=row['Artist']))
        self.assertEqual([''], test_track_uris)

    #TODO this is currently failing as spotify returns different URIs for the same track
    #why are these not unique? This could lead to errors...
    def test_only_write_one_song_if_duplicate_uris_exist(self):
        tracks = {'Song': ['Pigs... (In There)', 'Chika (Scared)', 'Stop the Dams', 'Stop the Dams'],
                  'Artist': ['Robert Wyatt', 'Fred again..', 'Gorillaz', 'Gorillaz'],
                  'Date': ['1', '1', '1', '1']}
        tracks_df = pd.DataFrame(tracks)
        show_name = 'test_tracks'
        tracks_df.to_csv(f'{show_name}.csv', index=False)
        URIs = ['spotify:track:33HqYo8b3I9xPdEALEdFFi', 'spotify:track:4pDE8vhtcEb90Eq2SvdWyB',
                'spotify:track:72nAHrq1bR6iDvng3qMpVf']

        CP = cp.CreatePlaylist()
        track_uris = CP.fetch_track_uris(show_name=show_name, track_uris_csv_exists=False)

        # Once test starts passing move the cleanup underneath...
        os.remove(f'{show_name}.csv')
        os.remove(f'{show_name}_track_uris.csv')

        self.assertEqual(URIs, track_uris)

    def test_filter_unknown_song(self):
        tracks = {'Date': ['17th-february-2020', '14th-february-2020'],
                  'Song': ['Unknown', 'What Colour Is Love'],
                  'Artist': ['Francis', 'Terry Callier']}
        tracks_df = pd.DataFrame(tracks)
        test_tracks =  {'Date': ['14th-february-2020'],
                  'Song': ['What Colour Is Love'],
                  'Artist': ['Terry Callier']}
        test_df = pd.DataFrame(test_tracks)

        CP = cp.CreatePlaylist()
        filtered_df = CP.filter_unknowns(library=tracks_df).reset_index(drop=True)

        self.assertTrue(test_df.equals(filtered_df))

    def test_filter_unknown_artist(self):
        tracks = {'Date': ['17th-february-2020', '14th-february-2020'],
                  'Song': ['my track', 'What Colour Is Love'],
                  'Artist': ['Unknown', 'Terry Callier']}
        tracks_df = pd.DataFrame(tracks)
        test_tracks =  {'Date': ['14th-february-2020'],
                  'Song': ['What Colour Is Love'],
                  'Artist': ['Terry Callier']}
        test_df = pd.DataFrame(test_tracks)

        CP = cp.CreatePlaylist()
        filtered_df = CP.filter_unknowns(library=tracks_df).reset_index(drop=True)

        self.assertTrue(test_df.equals(filtered_df))

    def test_add_uri_to_library(self):
        tracks = {'Song': ['Pigs... (In There)', 'Chika (Scared)', 'Stop the Dams'],
                  'Artist': ['Robert Wyatt', 'Fred again..', 'Gorillaz']}
        tracks_df = pd.DataFrame(tracks)
        test_tracks = {'Song': ['Pigs... (In There)', 'Chika (Scared)', 'Stop the Dams'],
                       'Artist': ['Robert Wyatt', 'Fred again..', 'Gorillaz'],
                       'track_uri': ['spotify:track:33HqYo8b3I9xPdEALEdFFi', 'spotify:track:4pDE8vhtcEb90Eq2SvdWyB',
                                     'spotify:track:72nAHrq1bR6iDvng3qMpVf']}
        CP = cp.CreatePlaylist()
        new_library = CP.add_uris_to_lib(library=tracks_df).reset_index(drop=True)
        test_df = pd.DataFrame(test_tracks)
        self.assertTrue(test_df.equals(new_library))


if __name__ == '__main__':
    unittest.main()
