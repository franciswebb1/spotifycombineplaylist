from app import playlist_scraper, file_transfer
from resources import constants


class TrackCount:
    def __init__(self, tablename, localhost=False, index=10):
        self.tablename = tablename
        self.localhost = localhost
        self.index = index
        self.d = {}

    def scrape_from_playlist(self):
        sp = playlist_scraper.ScrapePlaylist()
        df = sp.track_artist_count_dict(name=self.tablename, index=self.index)
        ta = playlist_scraper.TrackAnalysis()
        return ta.sort_df(df=df)

    def write_to_db(self):
        fileTransfer = file_transfer.FileTransfer(tablename=self.tablename, localhost=self.localhost, object=object)
        fileTransfer.file_transfer()

    def run_track_count(self):
        df = self.scrape_from_playlist()
        df.reset_index(inplace=True)
        df.to_excel('tracks_'+self.tablename+'.xlsx')
        self.d = df.to_dict(orient='records')
        self.write_to_db()


if __name__ == '__main__':
    trackCount = TrackCount(tablename=constants.TABLENAME, localhost=constants.LOCALHOST, index=constants.INDEX)
    trackCount.run_track_count()
