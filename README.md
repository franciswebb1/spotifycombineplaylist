# README #

This code can :

* Merge existing Spotify playlists into one single playlist. 

* Scrape public playlist info and write out to a dynamodb. This looks for all playlists of a given name and combines all information possible.

* Generate a new spotify playlist based on a track list file.

Currently running with Python 3.6. Authentication code must be updated each time the program is run - it times out after 
around 15 minutes. 

### Update 28/04/2020 - Code to create NTS playlists will no longer be updated, it lives here - https://bitbucket.org/franciswebb1/nts/src/master/ ###

### How do I get set up? ###

* Install all packages found in 'requirements.txt'
* Set up a new authentication token - https://developer.spotify.com/console/get-playlist/.
Once found, update `secrets.py` with your relevant spotify user id and authentication token.


*NOTE* The authentication token will expire after around 15 minutes, currently working to get this to be an automatic process.


### Current State ###

* Currently running main.py will scrape public playlist and combine song information
* To run spotify playlist generation code, this can be done directly in create_playlist.py. Future work will be to make this clearer

### How do I write output to a dynamodb on docker? ###
* Pull dynamodb image down from docker hub: ` docker pull amazon/dynamodb-local `
* Run this image as a container exposed on Port 8000: ` docker run --name dynamodb -p "8000:8000" -d amazon/dynamodb-local `
* Fetch new spotify authentication token and update `secrets.py`
* Build local Dockerfile for the application: ` docker build -t my-app . `
* Run this image as a container, linking it to our dynamodb container: `docker run --link dynamodb:localhost -ti my-app`
* *NOTE* If you would like to write to an external db, remove the endpoint url in `file_transfer.py`

### How do I run as a stack file?  ###
* `docker-compose up` should run the compose yml file as a stack.
* `docker stack deploy -c docker-compose.yml stack_name` will deploy the stack to a docker swarm. Note, you need to initialise a Docker swarm first (`docker swarm init`).

### Not using docker? ###

* You can run dynamodb locally by following these steps - https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html

### I want to produce a spotify playlist from an existing set of tracks ###

* Want to scrape the tracks from an NTS show? Visit "https://bitbucket.org/franciswebb1/nts/src/master/" to get started. If not don't worry! This code will work with any correctly formatted track list csv file.

* Copy across relevant csv file into the `app` repository, ensuring it has 'Song' and 'Artist' headers.

* Decide on output name. This is based on radio shows from "https://www.nts.live/about" but can be anything you would like!

* Run `create_playlist.py`. If there is a large amount of tracks it is possible the secrets key will expire whilst running. If this occurs, you can read in the original csv file but use the skiprows argument to bypass existing work carried out.

### Useful links ###

* Inspiration
This project is based loosely on the work carried out here - https://github.com/TheComeUpCode/SpotifyGeneratePlaylist.


### Backlog ###
* Remove duplicate song entries.
* User input names for source and target playlists.
* Update angular front end